package com.hcl.Movieticket_Redleaf.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.Movieticket_Redleaf.model.Movie;
import com.hcl.Movieticket_Redleaf.repository.MovieticketRedleafRepository;

@Service
public class MovieticketRedleafService {

	@Autowired
	MovieticketRedleafRepository Mvtrepo;

	// create
	public Movie createMovie(Movie Mv) {
		return Mvtrepo.save(Mv);
	}

	// read
	public List<Movie> DisplayAllMovie() {
		return Mvtrepo.findAll();
	}
	// update    

	public Movie updateMovie(Integer id,Movie Mv) 
	{
		Movie m = Mvtrepo.findById(id).get();
		m.setMoviename(Mv.getMoviename());
		m.setMoviegenre(Mv.getMoviegenre());
		m.setMovielanguage(Mv.getMovielanguage());
		m.setMovierating(Mv.getMovierating());
		m.setTicketprice(Mv.getTicketprice());
		
		return Mvtrepo.save(m);
	}

	// Delete
	public void DeleteMovie(Integer Id)
	{
		Mvtrepo.deleteById(Id);
	}
}
