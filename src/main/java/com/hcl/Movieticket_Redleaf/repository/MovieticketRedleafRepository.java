package com.hcl.Movieticket_Redleaf.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hcl.Movieticket_Redleaf.model.Movie;

@Repository
public interface MovieticketRedleafRepository extends JpaRepository<Movie, Integer> {

}
