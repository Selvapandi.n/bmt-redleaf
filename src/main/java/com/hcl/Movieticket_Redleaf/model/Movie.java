package com.hcl.Movieticket_Redleaf.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Movieticket_table")
public class Movie {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int movie_id;
	@Column(name="Movie_name")
	
	private String moviename;
	@Column(name="Movie_genre")
	
	private String moviegenre;
	@Column(name="Movie_language")
	private String movielanguage;
	@Column(name="Movie_rating")
	 private int movierating;
	@Column(name="Ticket_price")
	private Double ticketprice;
	public int getMovie_id() {
		return movie_id;
	}
	public void setMovie_id(int movie_id) {
		this.movie_id = movie_id;
	}
	public String getMoviename() {
		return moviename;
	}
	public void setMoviename(String moviename) {
		this.moviename = moviename;
	}
	public String getMoviegenre() {
		return moviegenre;
	}
	public void setMoviegenre(String moviegenre) {
		this.moviegenre = moviegenre;
	}
	public String getMovielanguage() {
		return movielanguage;
	}
	public void setMovielanguage(String movielanguage) {
		this.movielanguage = movielanguage;
	}
	public int getMovierating() {
		return movierating;
	}
	public void setMovierating(int movierating) {
		this.movierating = movierating;
	}
	public Double getTicketprice() {
		return ticketprice;
	}
	public void setTicketprice(Double ticketprice) {
		this.ticketprice = ticketprice;
	}
	public Movie(int movie_id, String moviename, String moviegenre, String movielanguage, int movierating,
			Double ticketprice) {
		super();
		this.movie_id = movie_id;
		this.moviename = moviename;
		this.moviegenre = moviegenre;
		this.movielanguage = movielanguage;
		this.movierating = movierating;
		this.ticketprice = ticketprice;
	}
	public Movie() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
}
