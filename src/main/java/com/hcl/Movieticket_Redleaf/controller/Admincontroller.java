package com.hcl.Movieticket_Redleaf.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.Movieticket_Redleaf.model.Movie;
import com.hcl.Movieticket_Redleaf.service.MovieticketRedleafService;

@RestController
@RequestMapping("/movie")
public class Admincontroller {

	@Autowired
	MovieticketRedleafService Mvtserv;
	// create
	@PostMapping("/add")
	public String createMovie(@RequestBody Map<String, Object> payload) 
	{
		Movie m = new Movie();
		m.setMoviename(payload.get("moviename").toString());
		m.setMoviegenre(payload.get("moviegenre").toString());
		m.setMovielanguage(payload.get("movielanguage").toString());
		m.setMovierating((int) payload.get("movierating"));
		m.setTicketprice((double) payload.get("ticketprice"));
		Mvtserv.createMovie(m);
	    return "New Movie posted";
	    
	}
	
	//read
	@GetMapping("/view")
	public List<Movie> DisplayAllMovie()
	
   {
		return Mvtserv .DisplayAllMovie();
		
   }
		
		
		
   //update
   @PutMapping("/modify/{id}")
 public  String updateMovie(@PathVariable Integer id,@RequestBody Map<String, Object> payload)
		{
			Movie m=new Movie();
		   m.setMoviename(payload.get("moviename").toString());
		   m.setMoviegenre(payload.get("moviegenre").toString());
		   m.setMovielanguage(payload.get("movielanguage").toString());
		   m.setMovierating((int)payload.get("movierating"));
		   m.setTicketprice((double)payload.get("ticketprice"));
		   Mvtserv.updateMovie(id, m);
		   return "Movie Details Updated";

		}
   
   //Delete
   @DeleteMapping("/remove/{id}")
  public String DeleteMovie(@PathVariable Integer id) {
   Mvtserv.DeleteMovie(id);
   return "Movie Details Deleted";
  }
   }
